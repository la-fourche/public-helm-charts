# PeopleVOX Subscriptions

[![pipeline status](https://gitlab.com/la-fourche/public-helm-charts/badges/master/pipeline.svg)](https://gitlab.com/la-fourche/public-helm-charts/-/commits/master)

PeopleVOX (also called PVX) as a WMS (Warehouse Management System).

PVX suffer from the lack of a proper way to handle and manage webhook subscriptions.
To subscribe to PVX, you must use the SOAP API, and there is no methods to get a list of the active subscriptions.

To ease the use of the PVX subscriptions, we developed a Kubernetes CRD and its operator. It keeps track of the previously created subscription in Kubernetes, and helps you manage it, by creating, updating or deleting your subscriptions with Kubernetes resources.

You can now manage your subscriptions as a resource in your project Helm chart.

## Getting Started

### Install the Helm Chart Repository

```bash
helm repo add lafourche https://la-fourche.gitlab.io/public-helm-charts
```

### Install the Helm Chart

```bash
helm install myrelease lafourche/peoplevox-subscriptions
```

## More about the Operator

You can find more about the Operator in the folling project: [PeopleVOX Subscriptions Kube Operator](https://gitlab.com/la-fourche/infra/pvx-subscriptions-kube-operator).

## More about La Fourche

Come get an eye on what we do, as developers, and as world citizens:

* [La Fourche](https://lafourche.fr)
* [Jobs](https://www.welcometothejungle.com/fr/companies/la-fourche)
